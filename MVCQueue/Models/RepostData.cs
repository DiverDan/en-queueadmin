﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using log4net;


namespace MVCQueue.Models
{
    public class RepostData
    {
        public string QformUid { get; set; }
        public string QFormName { get; set; }
        public DateTime InsertDate { get; set; }
        public string PartnerUid { get; set; }
        public string TrusteeUid { get; set; }
        public string Trusteeid { get; set; }
        private readonly static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal string Repost(string id, string trusteeid)
        {
            const string storedProcedureName = "QUEUE.pRepostQformsByTrusteeId";
            try
            {
                string result;
                using (
                    var connection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LendXConnectionString"].ConnectionString))
                using (var command = new SqlCommand())
                {
                    command.CommandText = storedProcedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;
                    command.Parameters.Add(new SqlParameter("@iTrusteeId", trusteeid) { DbType = DbType.Int32 });
                    command.Parameters.Add(new SqlParameter("@iQformName", id));
                    command.Parameters.Add("@r", SqlDbType.VarChar, 300).Direction = ParameterDirection.Output;
                    connection.Open();
                    command.ExecuteNonQuery();
                    result = Convert.ToString(command.Parameters["@r"].Value);
                    connection.Close();
                }
                Log.InfoFormat("Sproc: " + storedProcedureName + " @iTrusteeId = " + trusteeid + " @iQformName " + id + " executed by user " +
                                HttpContext.Current.User.Identity.Name);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data from sproc: " + storedProcedureName + " @iTrusteeId = " + trusteeid + " @iQformName " + id + " by user " +
                                HttpContext.Current.User.Identity.Name, ex);
            }
          return null;
        }

        internal List<RepostData> Get(string[] qformName, string trusteeid, string startdate)
        {
            var result = string.Empty;
            if (qformName != null)
            {
                result= string.Join(",", qformName);
            }
            const string storedProcedureName = "QUEUE.pGetQformsByTrusteeId";
            try
            {
                var listOfQfs = new List<RepostData>();
                var dt = new DataTable();
                //Add Columns
                dt.Columns.Add("QformName");
                if(qformName != null)
                    foreach (var id in qformName)
                    {
                        dt.Rows.Add(id);
                    }
                using (
                    var connection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LendXConnectionString"].ConnectionString))
                using (var command = new SqlCommand())
                {
                    command.CommandText = storedProcedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;
                    command.Parameters.Add(new SqlParameter("@iTrusteeId", trusteeid));
                    if (startdate != null)
                    {
                        command.Parameters.Add(new SqlParameter("@iStartDatetime", Convert.ToDateTime(startdate).Date)
                        {
                            IsNullable = true,
                            SqlDbType = SqlDbType.DateTime
                        });
                        command.Parameters.Add(new SqlParameter("@iEndDatetime", Convert.ToDateTime(startdate).Date.AddDays(1))
                        {
                            IsNullable = true,
                            SqlDbType = SqlDbType.DateTime
                        });
                    }
                       
                    
                    var tvparam = command.Parameters.AddWithValue("@iQformName", dt);
                    tvparam.SqlDbType = SqlDbType.Structured;
                    connection.Open();
                    var data = command.ExecuteReader();
                    while (data.Read())
                    {
                        var q = new RepostData
                        {
                            QformUid = Convert.ToString(data["QformUID"]),
                            InsertDate = Convert.ToDateTime(data["InsertDateTime"]),
                            QFormName = Convert.ToString(data["QformName"]),
                            PartnerUid = Convert.ToString(data["PartnerUid"]),
                            TrusteeUid = Convert.ToString(data["TrusteeUid"]),
                            Trusteeid = Convert.ToString(data["Trusteeid"])
                        };
                        listOfQfs.Add(q);
                    }
                    connection.Close();
                }
                
                Log.InfoFormat("Sproc: " + storedProcedureName + " @iTrusteeId = " + trusteeid + " @iQformName " + result + " executed by user " +
                                HttpContext.Current.User.Identity.Name);
                return listOfQfs;
                
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data from sproc: " + storedProcedureName + " @iTrusteeId = " + trusteeid + " @iQformName " + result + " by user " +
                                 HttpContext.Current.User.Identity.Name, ex);
                return null;
            }
            
        }



        internal string GetTransaction(string id, string trusteeuid, string partneruid)
        {
            const string storedProcedureName = "Queue.pGetTransactionResultsForRepost";
            try
            {
                string result;
                using (
                    var connection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LendXConnectionString"].ConnectionString))
                using (var command = new SqlCommand())
                {
                    command.CommandText = storedProcedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;
                    command.Parameters.Add(new SqlParameter("@TrusteeUID", trusteeuid));
                    command.Parameters.Add(new SqlParameter("@QformUID", id));
                    command.Parameters.Add(new SqlParameter("@PartnerUID", partneruid));
                    command.Parameters.Add("@OfferResult", SqlDbType.VarChar, 300).Direction = ParameterDirection.Output;
                    connection.Open();
                    command.ExecuteNonQuery();
                    result = Convert.ToString(command.Parameters["@OfferResult"].Value);
                    connection.Close();
                }
                Log.InfoFormat("Sproc: " + storedProcedureName + " @iTrusteeId = " + trusteeuid + " @QformUID " + id + " @PartnerUID = " + partneruid + " executed by user " +
                                HttpContext.Current.User.Identity.Name);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data from sproc: " + storedProcedureName + " @iTrusteeId = " + trusteeuid + " @QformUID " + id + " @PartnerUID = " + partneruid + " by user " +
                                 HttpContext.Current.User.Identity.Name, ex);
                return null;
            }
           
        }
    }
}