﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using log4net;

namespace MVCQueue.Models
{
    public class Queue
    {
        public DateTime InsertDate { get; set; }
        public string Sproc { get; set; }
        public string QformUid { get; set; }
        public string ErrorMessage { get; set; }
        public string DeadQName { get; set; }
        public string QFormName { get; set; }
        private readonly static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Queue> Get(string queueName)
        {
            try
            {
                var listOfFiles = new List<Queue>();
                queueName = string.IsNullOrEmpty(queueName)
                    ? "where deadQName = 'RequestFormDead'"
                    : string.Format("where deadQName = '{0}'", queueName);
                var query = "Select * from Queue.vGetDeadQueueInfo WITH (nolock)" + queueName;
                GetQueueData(listOfFiles, query);
                Log.InfoFormat("Fetching Data for QueueName: " + queueName + " by user " +
                                HttpContext.Current.User.Identity.Name);
                return listOfFiles;
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data for QueueName: " + queueName + " by user " +
                                HttpContext.Current.User.Identity.Name,ex);
            }

            return null;
        }

        private static void GetQueueData(ICollection<Queue> listOfFiles, string query)
        {
            try
            {
                using (
                    var connection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["LendXConnectionString"].ConnectionString))
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var dataDead = command.ExecuteReader();
                    while (dataDead.Read())
                    {
                        var q = new Queue
                        {
                            ErrorMessage = Convert.ToString(dataDead["errorMessage"]),
                            QformUid = Convert.ToString(dataDead["QformUID"]),
                            InsertDate = Convert.ToDateTime(dataDead["InsertDateTime"]),
                            Sproc = Convert.ToString(dataDead["sproc"]),
                            DeadQName = Convert.ToString(dataDead["deadQName"]),
                            QFormName = Convert.ToString(dataDead["QformName"])
                        };
                        listOfFiles.Add(q);
                    }
                    connection.Close();
                }
                Log.InfoFormat("Fetching Data from query : " + query + " by user " +
                                HttpContext.Current.User.Identity.Name);
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data from query: " + query + " by user " +
                                HttpContext.Current.User.Identity.Name, ex);
            }
           
        }



        internal void PerformDelete(string id)
        {
            var query = string.Format("Select sproc from Queue.vGetDeadQueueInfo (nolock) where QFormUID = '{0}'", id);
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LendXConnectionString"].ConnectionString))
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var dataDead = command.ExecuteReader();
                    while (dataDead.Read())
                    {
                        using (var deleteCommand = new SqlCommand(dataDead["sproc"].ToString(), connection))
                        {
                            deleteCommand.ExecuteNonQuery();
                        }
                    }
                    connection.Close();
                }
                Log.InfoFormat("Fetching Data from query : " + query + " by user " +
                                HttpContext.Current.User.Identity.Name);
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data from query: " + query + " by user " +
                               HttpContext.Current.User.Identity.Name, ex);
            }
           
        }

        internal List<Queue> GetQforms(string ids)
        {
            var query = string.Format("Select * from Queue.vGetDeadQueueInfo (nolock) where convert(nvarchar(1000), QformUID) IN ( '{0} ) or QFormName IN ( '{1} )", ids, ids);
            try
            {
                var listOfFiles = new List<Queue>();
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LendXConnectionString"].ConnectionString))
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var dataDead = command.ExecuteReader();
                    while (dataDead.Read())
                    {
                        var q = new Queue
                        {
                            ErrorMessage = Convert.ToString(dataDead["errorMessage"]),
                            QformUid = Convert.ToString(dataDead["QformUID"]),
                            InsertDate = Convert.ToDateTime(dataDead["InsertDateTime"]),
                            Sproc = Convert.ToString(dataDead["sproc"]),
                            DeadQName = Convert.ToString(dataDead["deadQName"]),
                            QFormName = Convert.ToString(dataDead["QformName"])
                        };
                        listOfFiles.Add(q);
                    }
                    connection.Close();
                }
                Log.InfoFormat("Fetching Data from query : " + query + " by user " +
                                HttpContext.Current.User.Identity.Name);
                return listOfFiles;
            }
            catch (Exception ex)
            {
                Log.Error("Error Fetching Data from query: " + query + " by user " +
                              HttpContext.Current.User.Identity.Name, ex);
                return null;
            }
           
        }
    }
}