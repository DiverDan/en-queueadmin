﻿var app = angular.module("QueueManagementApp", ['ngTouch', "ui.grid", 'ui.grid.pagination', 'ui.grid.selection', 'ngMessages']);

app.controller("QueueController", ["$scope", "$http", '$log', '$timeout', 'uiGridConstants', function ($scope, $http, $log, $timeout, uiGridConstants) {
    $scope.gridScope = $scope;
    var url = js.settings.HostName;
    $scope.hostname = url;

    $scope.queueName = '0';
    $scope.gridOptions = {
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter: true,
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 25,
        virtualizationThreshold: 25
    };

    $scope.deleteRow = function (row) {
        var index = $scope.gridOptions.data.indexOf(row);
        $http.post(url + "/Queue/DeleteSelected", { ids: row.QformUid, queueName: row.DeadQName }).
            success(function () {
                $scope.gridOptions.data.splice(index, 1);
            }).error(function (data) {
                alert(data);
            });
    };

    
    $scope.getSelectedRows = function () {
        $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
        $scope.mySelectedRows.forEach(function(obj) {
            $scope.deleteRow(obj);
        });
    };


    $scope.getQformIds = function () {
        var result = $scope.qformIds.replace(/[^a-z0-9-,]/gi, '');
        $http.post(url + "/Queue/SearchQforms", { ids: result }).
            success(function (result) {
                debugger;
                $scope.gridScope = $scope;
                $scope.queuedata = result;
                $scope.gridOptions = {
                    data: result,
                    enableRowSelection: true,
                    multiSelect: true,
                    columnDefs: [
                    {
                        field: "InsertDate",
                        displayName: "Insert Date Time"
                    },
                    {
                        field: "QformUid",
                        displayName: "QformUID"
                    },
                    {
                        field: "QFormName",
                        displayName: "QFormName"

                    },
                    {
                        field: "ErrorMessage",
                        displayName: "Error Message",
                        cellTooltip:
                            function (row) {
                                return row.entity.ErrorMessage;
                            }
                    },
                    {
                        field: "DeadQName",
                        displayName: "Dead Queue Name"
                    },
                    {
                        field: "delete",
                        cellTemplate: "<button id=\"deletebtn\" type=\"button\" class=\"btn-small glyphicon glyphicon-trash\" ng-click=\"grid.appScope.deleteRow(row.entity)\" ></button>"
                    }]
                }
                $scope.gridOptions.multiSelect = true;
                $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
                $timeout(function () {
                    //if ($scope.gridApi.selection.selectRow) {
                    //    $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
                    //}
                });

            }).
        error(function (data) {
            alert(data);
        });
    };



    $scope.getQueueinfo = function () {
        $http.post(url + "/Queue/FetchQueueData", { queueName: $scope.queueName }).
            success(function (result) {
                $scope.gridScope = $scope;
                $scope.queuedata = result;
                $scope.gridOptions = {
                    data: result,
                    enableRowSelection: true,
                    multiSelect: true,
                    columnDefs: [
                    {
                        field: "InsertDate",
                        displayName: "Insert Date Time"
                    },
                    {
                        field: "QformUid",
                        displayName: "QformUID"
                    },
                    {
                        field: "QFormName",
                        displayName: "QFormName"
                        
                    },
                    {
                        field: "ErrorMessage",
                        displayName: "Error Message",
                        cellTooltip:
                            function(row) {
                                return row.entity.ErrorMessage;
                            }
                    },
                    {
                        field: "DeadQName",
                        displayName: "Dead Queue Name"
                    },
                    {
                        field: "delete",
                        cellTemplate: "<button id=\"deletebtn\" type=\"button\" class=\"btn-small glyphicon glyphicon-trash\" ng-click=\"grid.appScope.deleteRow(row.entity)\" ></button>"
                    }]
                }
                $scope.gridOptions.multiSelect = true;
                $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
                $timeout(function () {
                    //if ($scope.gridApi.selection.selectRow) {
                    //    $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
                    //}
                });

            }).
        error(function (data) {
            alert(data);
        });
    };

    $scope.info = {};

   

    $scope.selectAll = function () {
        $scope.gridApi.selection.selectAllRows();
        $scope.mySelectedRows = $scope.gridApi.selection.selectAllRows().length;
    };

    $scope.clearAll = function () {
        $scope.gridApi.selection.clearSelectedRows();
        $scope.mySelectedRows = 0;
    };

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function () {
            $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
        });
        gridApi.pagination.on.paginationChanged($scope, function() {
            $scope.gridOptions.virtualizationThreshold = $scope.gridOptions.paginationPageSize;
        });
    };



}]);