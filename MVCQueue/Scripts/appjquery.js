﻿
$(document).ready(function () {
    $("#QueueGrid th").each(function () {
        if ($.trim($(this).text().toString().toLowerCase()) === "{checkall}") {
            $(this).text('');
            $("<input/>", { type: "checkbox", id: "cbSelectAll", value: "" }).appendTo($(this));
        }
    });

    $("#cbSelectAll").on("click", function () {
        var ischecked = this.checked;
        $("#QueueGrid").find("input:checkbox").each(function () {
            this.checked = ischecked;
        });
    });

    $("input[name='ids']").click(function () {
        var totalRows = $("#QueueGrid td :checkbox").length;
        var checked = $("#QueueGrid td :checkbox:checked").length;

        if (checked === totalRows) {
            $("#QueueGrid").find("input:checkbox").each(function () {
                this.checked = true;
            });
        }
        else {
            $("#cbSelectAll").removeAttr("checked");
        }
    });




});


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    return "1";

}