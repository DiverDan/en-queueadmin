﻿$(document).ready(function () {
    //Attaching the change event of dropdown list
    $('#queueName').change(function () {
        $("[form-row]").hide();
        $('[form-row][data-type="' + $(this).val() + '"]').show();
    });
});



$(document).ready(function () {
    $('.chkdeleteSelected').click(function () {
        var buttonsChecked = $('.chkdeleteSelected:checked');
        if (buttonsChecked.length) {
            $('#deleteSelected').removeAttr('disabled');
        }
        else {
            $('#deleteSelected').attr('disabled', 'disabled');
        }
    });
});


$(document).ready(function () {
    $('.chkAll').click(function () {
        var buttonsChecked = $('.chkAll:checked');
        if (buttonsChecked.length) {
            $('.chkdeleteSelected').prop('checked', true);
            $('#deleteSelected').removeAttr('disabled');
        }
        else {
            $('.chkdeleteSelected').attr('checked', false);
            $('#deleteSelected').attr('disabled', 'disabled');
        }
    });
});