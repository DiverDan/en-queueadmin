﻿var app = angular.module("RepostManagementApp", ['ngTouch', "ui.grid", 'ui.grid.pagination', 'ui.grid.selection', 'ngMessages','ngAnimate', 'ui.bootstrap']);

app.controller("RepostController", ["$scope", "$http", '$log', '$timeout', 'uiGridConstants', function ($scope, $http, $log, $timeout, uiGridConstants) {
    var url = js.settings.HostName;
    $scope.hostname = url;
    $scope.gridScope = $scope;
    $scope.mySelectedRows = 0;
    $scope.gridOptions = {
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter: true,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        virtualizationThreshold: 25
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.today = function () {
        $scope.dt = new Date();
    };
    //$scope.today();

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    //$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.formats = ['dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events =
      [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
      ];

    $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    };

    $scope.repostRow = function (row) {
        $http.post(url+ "/Repost/RepostSelected", { ids: row.QFormName, trusteeid: row.Trusteeid }).
            success(function (data) {
                row.Reposted = data;
            }).error(function (data) {
                alert(data);
            });
    };

    $scope.getSelectedRows = function () {
        $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
        $scope.mySelectedRows.forEach(function (obj) {
            $scope.repostRow(obj);
        });
    };

    $scope.GetTransaction = function(row) {
        $http.post(url + "/Repost/GetTransactionResultForSelected", { ids: row.QformUid, trusteeuid: row.TrusteeUid, partneruid: row.PartnerUid }).
           success(function (data) {
               row.TransactionResult = data;
           }).error(function (data) {
               alert(data);
           });
    }

    $scope.getTransactionsForSelectedRows = function() {
        $scope.mySelectedRowsForTransaction = $scope.gridApi.selection.getSelectedRows();
        $scope.mySelectedRowsForTransaction.forEach(function (obj) {
            $scope.GetTransaction(obj);
        });
    };
    
    $scope.getQformIds = function () {
        debugger;
        var result;
        if ($scope.qformNames != null)
            result = $scope.qformNames.replace(/[^a-z0-9-,]/gi, "");
        $http.post(url + "/Repost/SearchQformData", { ids: result, trusteeId: $scope.trusteeid, startDate: $scope.dt }).
            success(function (data) {
                debugger;
                $scope.gridScope = $scope;
                $scope.queuedata = data;
                $scope.gridOptions = {
                    data: data,
                    enableRowSelection: true,
                    multiSelect: true,
                    columnDefs: [
                    {
                        field: "QFormName",
                        displayName: "QFormName"

                    },
                    //{
                    //    field: "PartnerUid",
                    //    displayName: "PartnerUid"

                    //},
                    //{
                    //    field: "TrusteeUid",
                    //    displayName: "TrusteeUid"

                    //},
                    {
                        field: "Trusteeid",
                        displayName: "Trusteeid"

                    },
                    //{
                    //    field: "InsertDate",
                    //    displayName: "Insert Date Time"
                    //},
                    //{
                    //    field: "QformUid",
                    //    displayName: "QformUid"
                    //},
                    {
                        field: "Reposted",
                        displayName: "Reposted",
                        cellTooltip:
                            function(row) {
                                return row.entity.Reposted;
                            }
                    },

                    {
                        field: "TransactionResult",
                        displayName: "TransactionResult",
                        cellTooltip:
                            function (row) {
                                return row.entity.TransactionResult;
                            }
                    },

                    {
                        field: "Repost",
                        cellTemplate: "<button id=\"repostbtn\" type=\"button\" class=\"btn-small glyphicon glyphicon-repeat\" ng-click=\"grid.appScope.repostRow(row.entity)\" ></button>"
                    },
                    {
                        field: "TransactionDetails",
                        cellTemplate: "<button id=\"repostbtn\" type=\"button\" class=\"btn-small glyphicon glyphicon-zoom-in\" ng-click=\"grid.appScope.GetTransaction(row.entity)\" ></button>"
                    }
                    ]
                }
                $scope.gridOptions.multiSelect = true;
                $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
                $timeout(function () {
                    //if ($scope.gridApi.selection.selectRow) {
                    //    $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
                    //}
                });

            }).
        error(function (data) {
            alert(data);
        });
    };

    $scope.info = {};

    $scope.selectAll = function () {
        $scope.gridApi.selection.selectAllRows();
        $scope.mySelectedRows = $scope.gridApi.selection.selectAllRows().length;
    };

    $scope.clearAll = function () {
        $scope.gridApi.selection.clearSelectedRows();
        $scope.mySelectedRows = 0;
    };

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (){
            $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
        });
        gridApi.pagination.on.paginationChanged($scope, function () {
            $scope.gridOptions.virtualizationThreshold = $scope.gridOptions.paginationPageSize;
        });
    };

}]);