﻿using MVCQueue.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MVCQueue.Controllers
{
    public class QueueController : Controller
    {
        // GET: Queue
        public List<Queue> List = null;


        public ActionResult Index()
        {
            
            return View("Index", null);
        }

        //GET: Queue
        [HttpPost]
        [ActionName("FetchQueueData")]
        public dynamic Index(string queueName)
        {
             if (Session["Model"] != null)
            {
                List = (List<Queue>)Session["Model"];
                return JsonConvert.SerializeObject(List, Formatting.Indented);
            }
            var af = new Queue();
            List = af.Get(queueName);
            return JsonConvert.SerializeObject(List, Formatting.Indented);
        }


        [HttpPost]
        public ActionResult DeleteSelected(string[] ids, string queueName)
        {
            var delentry = new Queue();
            if (ids == null) return Redirect("~/Queue/Index");
            foreach (var id in ids)
            {
                delentry.PerformDelete(id);
            }
            return Redirect("~/Queue/Index");
        }

        public ActionResult Delete(string id)
        {
            Queue delentry = new Queue();
            delentry.PerformDelete(id);
            return Redirect("~/Queue/Index");
        }

        public dynamic SearchQforms(string ids)
        {
            ids = ids.Replace(",", "','");
            ids = ids + "'";
            var af = new Queue();
            List = af.GetQforms(ids);
            return JsonConvert.SerializeObject(List, Formatting.Indented);
        }

    }
}