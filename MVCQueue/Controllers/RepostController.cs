﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MVCQueue.Models;
using Newtonsoft.Json;

namespace MVCQueue.Controllers
{
    public class RepostController : Controller
    {
        // GET: Repost
        public List<RepostData> List = null;

        public ActionResult Index()
        {
            return View();
        }

        //GET: Queue
        [HttpPost]
        [ActionName("SearchQformData")]
        public dynamic Index(string ids, string trusteeid, string startDate)
        {
            if (Session["Model"] != null)
            {
                List = (List<RepostData>)Session["Model"];
                return JsonConvert.SerializeObject(List, Formatting.Indented);
            }
            var af = new RepostData();
            var idArray = ids!=null? ids.Split(','): null;
            List = af.Get(idArray, trusteeid, startDate);
            return JsonConvert.SerializeObject(List, Formatting.Indented);
        }



        [HttpPost]
        public dynamic RepostSelected(string[] ids, string trusteeid)
        {
            var entry = new RepostData();
            var result = string.Empty;
            if (ids == null) return Redirect("~/Repost/Repost");
            result = ids.Aggregate(result, (current, id) => current + entry.Repost(id, trusteeid));
            return JsonConvert.SerializeObject(result, Formatting.Indented);
        }


        [HttpPost]
        public dynamic GetTransactionResultForSelected(string[] ids, string trusteeuid, string partneruid)
        {
            var entry = new RepostData();
            var result = string.Empty;
            if (ids == null) return Redirect("~/Repost/Repost");
            result = ids.Aggregate(result, (current, id) => current + entry.GetTransaction(id, trusteeuid, partneruid));
            return JsonConvert.SerializeObject(result, Formatting.Indented);
        }

    }
}